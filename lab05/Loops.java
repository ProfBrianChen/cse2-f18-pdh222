///////////
//// CSE02 Lab05 Loops
/// Peter Hartshorne
/// 10/4/18
/// Asks the user for information about the courses they are taking
import java.util.Scanner;

public class Loops {
  // main method required for every Java program
   public static void main(String args[]) {
     Scanner input = new Scanner(System.in);
     
     System.out.println("What is the course number?");
     
     boolean correct = input.hasNextInt();
     int courseNumber = 0;
     while (correct == false){
        input.next();
        System.out.println("Wrong variable type; please input an integer.");
        correct = input.hasNextInt();
     }
     courseNumber = input.nextInt();
     
     System.out.println("What is the department name?");
     
     boolean correctString = input.hasNext();
     String department = "";
     while (correctString == false){
        input.next();
        System.out.println("Wrong variable type; please input a string.");
        correctString = input.hasNext();
     }
     department = input.next();
     
     System.out.println("How many times does the class meet each week?");
     
     boolean correctInt = input.hasNextInt();
     int meetingTimes = 0;
     while (correctInt == false){
        input.next();
        System.out.println("Wrong variable type; please input an integer.");
        correctInt = input.hasNextInt();
     }
     meetingTimes = input.nextInt();
     
      System.out.println("When does the class start? Type in a string as: Time am/pm (ex: 8 am)");
     
     boolean correctString2 = input.hasNext();
      String time = "";
     while (correctString2 == false){
        input.next();
        System.out.println("Wrong variable type; please input a string.");
        correctString2 = input.hasNext();
     }
     time = input.next();
     
     System.out.println("What is the name of the instructor?");
     
     boolean correctString3 = input.hasNext();
      String professorName = "";
     while (correctString3 == false){
        input.next();
        System.out.println("Wrong variable type; please input a string.");
        correctString3 = input.hasNext();
     }
     professorName = input.next();
     
      System.out.println("How many students are in the class?");
     
     boolean correctInt2 = input.hasNextInt();
      int students = 0;
     while (correctInt2 == false){
        input.next();
        System.out.println("Wrong variable type; please input an integer.");
        correctInt2 = input.hasNextInt();
     }
      students = input.nextInt();
     
     System.out.println("Course Number: " + courseNumber);
     System.out.println("Department Name: " + department);
     System.out.println("Meeting Times per Week: " + meetingTimes);
     System.out.println("Class Time: " + time);
     System.out.println("Instructor Name: " + professorName);
     System.out.println("Students in Class: " + students);
     
     
  
   } //end of main method
} //end of class