///////////
//// CSE02 Lab06 PatternD
/// Peter Hartshorne
/// 10/11/18
/// Prints out a pyramid of integers based on user input
import java.util.Scanner;

public class PatternD {
  // main method required for every Java program
   public static void main(String args[]) {
     Scanner input = new Scanner(System.in);
     int numRows = 0;
     System.out.println("How many rows would you like the pyramid to be (Enter an integer 1-10): ");
     boolean correct = false;
     
      while (correct == false){
        if (input.hasNextInt()){
          numRows = input.nextInt();
          if (numRows < 1 || numRows > 10){
            System.out.println("Variable is outside of range.");
          }
          else {
            correct = true;
          }
        }
       else
       {
         System.out.println("Wrong variable type.");
         input.next();
       }
     }

     int k = numRows;
      for (int i = 1; i <= numRows; i++){
       for (int j = k; j >= 1; j--){
         System.out.print(j + " ");
       }
        k--;
        System.out.println("");
     }
     
     
   } //end of main class
} //end of main method