///////////
//// CSE02 Lab08 Arrays
/// Peter Hartshorne
/// 11/08/18
/// Makes two arrays
import java.util.Scanner;

public class Arrays {
  // main method required for every Java program
   public static void main(String args[]) {
    int[] one = new int [ 100 ];
    int[] two = new int [ 100 ];
    int[] three = new int [ 100 ];
    
     System.out.println("Array one holds:");
    for (int i = 1; i < one.length; i++){
      one[i] = (int)(Math.random()*100);
      System.out.println(one[i]);
    }
    
    for (int j = 1; j < two.length; j++){
      two[j] = j;
    }
    
    for (int k = 1; k < 100; k++){
      for (int b = 1; b < 100; b++){
       if (two[k] == one[b]){
         three[k] += 1;
       }
      }
      System.out.println(k + " occurs " + three[k] + " times");
    }
   } //end of main class
} //end of main method