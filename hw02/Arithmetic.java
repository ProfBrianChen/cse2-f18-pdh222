///////////
//// CSE 02 Arithmetic
/// Peter Hartshorne
/// 9/11/18
/// Computes the total cost of items bought including PA sales tax.
public class Arithmetic {
  // main method required for every Java program
   public static void main(String args[]) {   
         //Input Variables
         int numPants = 3; //Number of pairs of pants
         double pantsPrice = 34.98; //Cost per pair of pants
         int numShirts = 2; //Number of sweatshirts
         double shirtPrice = 24.99; //Cost per shirt
         int numBelts = 1; //Number of belts
         double beltCost = 33.99; //Cost per belt
         double paSalesTax = 0.06; //PA tax rate
     
         //Output Variables
         double totalCostOfPants; //Total cost of pants without tax
         double totalCostOfShirts; //Total cost of shirts without tax
         double totalCostOfBelts; //Total cost of belts without tax
         double salesTaxOfPants; //Sales tax of pants
         double salesTaxOfShirts; //Sales tax of shirts
         double salesTaxOfBelts; //Sales tax of belts
         double totalBeforeTax; //Sale total before tax is factored in
         double totalSalesTax; //Total sales tax that was applied to the purchase
         double totalAfterTax; //Sale total after tax is factored in
          
         //Calculations //NEED TO DIVIDE EVERYTHING BY 100 AND STUFF
         totalCostOfPants = numPants * pantsPrice; //The total cost of pants is equal to the price of one pair times the number of pairs bought
         totalCostOfShirts = numShirts * shirtPrice; //The total cost of shirts is equal to the price of one shirt times the number of shirts bought
         totalCostOfBelts = numBelts * beltCost; //The total cost of belts is equal to the price of one belt times the number of belts bought
     
         salesTaxOfPants = totalCostOfPants * paSalesTax; //The sales tax applied to all pants is equal to the total cost times the tax rate
           salesTaxOfPants = (int) (salesTaxOfPants * 100); //Multiplying the sales tax by 100, casting it as an int, then dividing by 100 removes excess decimals
           salesTaxOfPants = salesTaxOfPants / 100.0;
         salesTaxOfShirts = totalCostOfShirts * paSalesTax; //The sales tax applied to all shirts is equal to the total cost times the tax rate
           salesTaxOfShirts = (int) (salesTaxOfShirts * 100); //Multiplying the sales tax by 100, casting it as an int, then dividing by 100 removes excess decimals
           salesTaxOfShirts = salesTaxOfShirts / 100.0;
         salesTaxOfBelts = totalCostOfBelts * paSalesTax; //The sales tax applied to all belts is equal to the total cost times the tax rate
           salesTaxOfBelts = (int) (salesTaxOfBelts * 100); //Multiplying the sales tax by 100, casting it as an int, then dividing by 100 removes excess decimals
           salesTaxOfBelts = salesTaxOfBelts / 100.0;
     
         totalBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //The transaction cost before tax is equal to the total cost of all three items  
         totalSalesTax = salesTaxOfPants + salesTaxOfShirts + salesTaxOfBelts; //The total sales tax is equal to the sales tax applied to all three items
           totalSalesTax = (int) (totalSalesTax * 100); //Multiplying the sales tax by 100, casting it as an int, then dividing by 100 removes excess decimals
           totalSalesTax = totalSalesTax / 100.0;
         totalAfterTax = totalBeforeTax + totalSalesTax; //The transaction cost after tax is equal to the total cost of all three items added to the total sales tax 
           totalAfterTax = (int) (totalAfterTax * 100); //Multiplying the sales tax by 100, casting it as an int, then dividing by 100 removes excess decimals
           totalAfterTax = totalAfterTax / 100.0;
     
         //Output
         System.out.println("The total cost of the pants is $" + totalCostOfPants); //Outputs total cost of pants
         System.out.println("The total cost of the shirts is $" + totalCostOfShirts); //Outputs total cost of shirts
         System.out.println("The total cost of the belts is $" + totalCostOfBelts); //Outputs total cost of belts
         System.out.println("The sales tax applied to the pants is $" + salesTaxOfPants); //Outputs sales tax applied to pants
         System.out.println("The sales tax applied to the shirts is $" + salesTaxOfShirts); //Outputs sales tax applied to shirts
         System.out.println("The sales tax applied to the belts is $" + salesTaxOfBelts); //Outputs sales tax applied to belts
         System.out.println("The total cost of the transaction before tax is $" + totalBeforeTax); //Outputs total cost of transaction
         System.out.println("The total sales tax applied to the transaction is $" + totalSalesTax); //Outputs total sales tax of transaction
         System.out.println("The total cost of the transaction after tax is $" + totalAfterTax); //Outputs
 	}  //end of main method   
} //end of class