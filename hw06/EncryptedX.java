///////////
//// CSE02 Hw06 EncryptedX
/// Peter Hartshorne
/// 10/21/18
/// Prints out an X made of spaces based on user input for the size of the X.
import java.util.Scanner;

public class EncryptedX {
  // main method required for every Java program
   public static void main(String args[]) {
     Scanner input = new Scanner(System.in);
     int sideLength = 0; //initializes side length variable
     System.out.println("Enter an integer (0-100) for the dimension of the side of the square X: "); //asks the user to enter in an input between 0 - 100
     
     //code to run to make sure input is correct
     boolean correct = false;
     
      while (correct == false){
        if (input.hasNextInt()){ //if the user has entered an int
          sideLength = input.nextInt(); //the input is equal to side length
          if (sideLength < 0 || sideLength > 100){ //checks to see if variable is outside range
            System.out.println("Variable is outside of range.");
          }
          else {
            correct = true; //if variable is inside range it stops while loop
          }
        }
       else
       {
         System.out.println("Wrong variable type.");
         input.next(); //steps through wrong variable type
       }
     }

      for (int i = 1; i <= sideLength; i++){ //creates the columns
        for (int j = 1; j <= sideLength; j++){ //creates rows
         if (j == i || j + i == sideLength + 1) { //if column equals rows or row + columns = sideLength + 1 then print a space
           System.out.print(" ");
         }
         else {
           System.out.print("*"); //otherwise print a star
         }
        }
        System.out.println("");
     }
     
     
   } //end of main class
} //end of main method