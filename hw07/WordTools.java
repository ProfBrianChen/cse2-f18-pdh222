import java.util.Scanner;

///////////
//// CSE 02 Hw07 Word Tools.java
/// Peter Hartshorne
/// 10/29/18
/// Creates different string methods and utilizes them based on user input
public class WordTools {
  // main method required for every Java program
 public static void main(String args[]) {
  Scanner input = new Scanner(System.in); //initializes scanner
  String sentence = sampleText(); //sentence is equal to the output from method sampleText
  System.out.println("You entered: " + sentence);
  printMenu(sentence); //calls printMenu and sends it sentence
 }
  
  public static String sampleText() { //method to get string from user
    Scanner input = new Scanner(System.in);
    System.out.println("Please enter a string of your choosing: ");
    String sentence = input.nextLine();
    return sentence;
  }
  
  public static String printMenu(String sentence) { //print menu method
    Scanner input = new Scanner(System.in);
  System.out.println("MENU: ");
  System.out.println("c - find number of non-whitespace characters");
  System.out.println("w - number of words");
  System.out.println("f - find text");
  System.out.println("r - replace all !'s");
  System.out.println("s - shorten spaces");
  System.out.println("q - quit");
    
 //code to run to make sure user enters correct input
 boolean correct = false;
 String character = "";
 while (correct == false){
 character = input.nextLine();
 if ("c".equals(character) || "w".equals(character) || "f".equals(character) || "r".equals(character) || "s".equals(character) || "q".equals(character)){
   //only runs if user enters correct input
          System.out.println("You've selected " + character);
          correct = true;
          }
          else {
            System.out.println("This character is not one of the accepted options, please try again!");
          }
 }
    //if the user enters an accepted input, calls methods assigned to that input and sends it string sentence
   if ("c".equals(character)){
   getNumOfNonWSCharacters(sentence);
   }
    
    else if ("w".equals(character)){
   getNumOfWords(sentence);
   }
   
   else if ("f".equals(character)){
   System.out.println("Please enter a word that you want to find: ");
   String word = input.nextLine();
   findText(sentence, word); //sends find text method two inputs
   }
   
    else if ("r".equals(character)){
   replaceExclamation(sentence);
   }
   
   else if ("s".equals(character)){
   shortenSpace(sentence);
   }
  
  else if ("q".equals(character)){
   quit();
   }
    
    return character;
      }
  
  public static void quit(){ //quit method
    System.exit(0);
  }
  
  public static int getNumOfNonWSCharacters(String sentence) { //number of nonwhitespacecharacters method
    int j = 0;
    for (int i = 0; i <= sentence.length() - 1; i++){
      if (sentence.charAt(i) == ' '){} //if the char at i in the sentence is a space it adds one to the counter
      else {
        j++;
      }
    }
    System.out.println("The number of non-whitespace characters is: " + j);
    printMenu(sentence); //goes back to menu
    return j;
  }
  
  public static int getNumOfWords(String sentence) { //number of words method
    int j = 0;
    //this is saying that if chars are surrounded by either a period or a space then they will be counter as a word.
    for (int i = 1; i <= sentence.length() - 1; i++){
       if (sentence.charAt(i) == '.'){
          j++;
        }
      if (sentence.charAt(i) == ' '){
        if (sentence.charAt(i - 1) == '.'){}
        else{
        j++;
         }
        }
       }
    System.out.println("The number of words is: " + j);
    printMenu(sentence); //goes back to print menu
    return j;
    }
  
  public static String replaceExclamation(String sentence){ //replace all exclamation points with periods
    String newSentence = "";
    for (int i = 0; i <= sentence.length() - 1; i++){ //iterates through sentence and adds to a new string unless char at i is a !, then it adds a period
    if (sentence.charAt(i) == '!'){
      newSentence = newSentence + ".";
    }
   else {
     newSentence = newSentence + sentence.charAt(i);
   }
  }
    System.out.println("Edited text: " + newSentence); //prints out edited text
    printMenu(sentence); //returns to print menu
    return newSentence;
}
 
 public static String shortenSpace(String sentence){ //method to shorten spaces
   String newSentence = "";
   for (int i = 0; i <= sentence.length() - 1; i++){
    if (sentence.charAt(i) == ' ' && sentence.charAt(i+1) == ' '){ //iterates through sentence and if two spaces are found next to each other then the new string is created with only 1 space
    }
    else {
      newSentence = newSentence + sentence.charAt(i);
    }
 }
   System.out.println("Edited text: " + newSentence);
   printMenu(sentence); //returns to print menu
   return newSentence;
 }
 
 public static int findText(String sentence, String word){ //method to find a word in a string
  int j = 0;
   for (int i = 0; i <= sentence.length() - word.length(); i++){ //iterates through the sentence as long as i is less than sentence - word (to keep it in bounds)
     if (word.equals(sentence.substring(i, word.length() + i))){ //if the substring from i to wordlength is equal to the word, adds to counter
       j++;
     }
   }
  System.out.println("'" + word + "'" + " instances: " + j); //prints result
  printMenu(sentence); //returns to print menu
  return j;
  }

}
  