import java.util.Scanner;
///////////
//// CSE02 HW05
/// Peter Hartshorne
/// 10/7/18
/// Determines the probability of getting different poker hands
public class Hw05 {
  // main method required for every Java program
   public static void main(String args[]) {
     Scanner input = new Scanner(System.in);
     System.out.println("How many times would you like to generate hands? (Enter an integer): ");
     
     //code to make sure the input is of the correct type
     boolean correct = input.hasNextInt();
     int hands = 0;
     while (correct == false){
        input.next();
        System.out.println("Wrong variable type; please input an integer.");
        correct = input.hasNextInt();
     }
     hands = input.nextInt();
     
     int card = 0;
     int i = 0;
     String suit = "";
     String card1 = "";
     String card2 = "";
     String card3 = "";
     String card4 = "";
     String card5 = "";
     int card6 = 0;
     int card7 = 0;
     int card8 = 0;
     int card9 = 0;
     int card10 = 0;
     int onepair = 0;
     int twopair = 0;
     int threes = 0;
     int fours = 0;
     
     while (i < hands){
        card = (int)(Math.random()*51)+1; //Generates a random number between 1 and 52 (inclusive)
        
       //Suit Calculations
     if (1 <= card && card <= 13) { //If the card generated is between 1 and 13 then it is a diamond
       suit = "Diamonds"; //Assigns suit diamonds to "diamonds"
       }
     
     if (14 <= card && card <= 26){ //If the card generated is between 14 and 26 then it is a club
       suit = "Clubs"; //Assigns suit variable to "clubs"
     }
     
     if (27 <= card && card <= 39){ //If the card generated is between 27 and 39 then it is a heart
       suit = "Hearts"; //Assigns suit variable to "hearts"
       }
     
    if (40 <= card && card <= 52){ //If the card generated is between 40 and 52 then it is a spade
      suit = "Spades"; //Assigns suit variable to "spades"
     }
      
     //Identity Calculations
     card = card % 13; //Takes remainder of card / 13 to get the identity of the card
         card1 = card + suit;
         card6 = card;

      card1 = card2;
      while (card1.equals(card2)){
        card = (int)(Math.random()*51)+1; //Generates a random number between 1 and 52 (inclusive)
        
       //Suit Calculations
     if (1 <= card && card <= 13) { //If the card generated is between 1 and 13 then it is a diamond
       suit = "Diamonds"; //Assigns suit diamonds to "diamonds"
       }
     
     if (14 <= card && card <= 26){ //If the card generated is between 14 and 26 then it is a club
       suit = "Clubs"; //Assigns suit variable to "clubs"
     }
     
     if (27 <= card && card <= 39){ //If the card generated is between 27 and 39 then it is a heart
       suit = "Hearts"; //Assigns suit variable to "hearts"
       }
     
    if (40 <= card && card <= 52){ //If the card generated is between 40 and 52 then it is a spade
      suit = "Spades"; //Assigns suit variable to "spades"
     }
      
     //Identity Calculations
     card = card % 13; //Takes remainder of card / 13 to get the identity of the card
         card2 = card + suit;
         card7 = card;
     }
         card2 = card3;
         while(card2.equals(card3)){
         card = (int)(Math.random()*51)+1; //Generates a random number between 1 and 52 (inclusive)
        
       //Suit Calculations
     if (1 <= card && card <= 13) { //If the card generated is between 1 and 13 then it is a diamond
       suit = "Diamonds"; //Assigns suit diamonds to "diamonds"
       }
     
     if (14 <= card && card <= 26){ //If the card generated is between 14 and 26 then it is a club
       suit = "Clubs"; //Assigns suit variable to "clubs"
     }
     
     if (27 <= card && card <= 39){ //If the card generated is between 27 and 39 then it is a heart
       suit = "Hearts"; //Assigns suit variable to "hearts"
       }
     
    if (40 <= card && card <= 52){ //If the card generated is between 40 and 52 then it is a spade
      suit = "Spades"; //Assigns suit variable to "spades"
     }
      
     //Identity Calculations
     card = card % 13; //Takes remainder of card / 13 to get the identity of the card
         card3 = card + suit;
         card8 = card;
     }
         card3 = card4;
         while(card3.equals(card4)){
         card = (int)(Math.random()*51)+1; //Generates a random number between 1 and 52 (inclusive)
        
       //Suit Calculations
     if (1 <= card && card <= 13) { //If the card generated is between 1 and 13 then it is a diamond
       suit = "Diamonds"; //Assigns suit diamonds to "diamonds"
       }
     
     if (14 <= card && card <= 26){ //If the card generated is between 14 and 26 then it is a club
       suit = "Clubs"; //Assigns suit variable to "clubs"
     }
     
     if (27 <= card && card <= 39){ //If the card generated is between 27 and 39 then it is a heart
       suit = "Hearts"; //Assigns suit variable to "hearts"
       }
     
    if (40 <= card && card <= 52){ //If the card generated is between 40 and 52 then it is a spade
      suit = "Spades"; //Assigns suit variable to "spades"
     }
      
     //Identity Calculations
     card = card % 13; //Takes remainder of card / 13 to get the identity of the card
         card4 = card + suit;
         card9 = card;
     }
         card4 = card5;
         while(card4.equals(card5)){
         card = (int)(Math.random()*51)+1; //Generates a random number between 1 and 52 (inclusive)
        
       //Suit Calculations
     if (1 <= card && card <= 13) { //If the card generated is between 1 and 13 then it is a diamond
       suit = "Diamonds"; //Assigns suit diamonds to "diamonds"
       }
     
     if (14 <= card && card <= 26){ //If the card generated is between 14 and 26 then it is a club
       suit = "Clubs"; //Assigns suit variable to "clubs"
     }
     
     if (27 <= card && card <= 39){ //If the card generated is between 27 and 39 then it is a heart
       suit = "Hearts"; //Assigns suit variable to "hearts"
       }
     
    if (40 <= card && card <= 52){ //If the card generated is between 40 and 52 then it is a spade
      suit = "Spades"; //Assigns suit variable to "spades"
     }
      
     //Identity Calculations
     card = card % 13; //Takes remainder of card / 13 to get the identity of the card
         card5 = card + suit;
         card10 = card;
     }
        if (card6 == card7 || card6 == card8 || card6 == card9 || card6 == card10 || card7 == card8 || card7 == card9 || card7 == card10 || card8 == card9 || card8 == card10 || card9 == card10){
        onepair++;
        }
            
        if (card6 == card7 && card8 == card9 || card6 == card7 && card8 == card10 || card6 == card7 && card9 == card10 || card6 == card8 && card7 == card10 || card6 == card8 && card7 == card9 || card7 == card8 && card6 == card10 || card7 == card9 && card6 == card10){
        twopair++;
        }
        
        if (card6 == card7 && card7 == card8 || card6 == card7 && card7 == card9 || card6 == card7 && card7 == card10 || card6 == card8 && card8 == card9 || card6 == card8 && card8 == card10 || card6 == card9 && card9 == card10){
        threes++;
        }
        
        if (card6 == card7 && card7 == card8 && card8 == card9 || card7 == card8 && card8 == card9 && card9 == card10){
        fours++;
        }
       
        i++;
     }
  
     System.out.println("The number of hands is: " + hands);
     System.out.println("The probability of one pairs: " + (double) (onepair / hands));
     System.out.println("The probability of two pairs: " + (double) (twopair / hands));
     System.out.println("The probability of three of a kind: " + (double) (threes / hands));
     System.out.println("The probability of four of a kind: " + (double) (fours / hands));
     
   } //end of main method
} //end of class