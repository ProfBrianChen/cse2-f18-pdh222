import java.util.Scanner;

///////////
//// CSE02 HW03 Convert.java
/// Peter Hartshorne
/// 9/16/18
/// Obtains information on inches of rainfall and the number of affected acres from the user and computes cubic miles of rainfall
public class Convert {
  // main method required for every Java program
   public static void main(String args[]) {
     //User Inputs
     Scanner myScanner = new Scanner( System.in ); //Declares new scanner
     
     System.out.print("Enter the affected area in acres: "); //Asks the user for input on the number of affected acres
     double acresAffected = myScanner.nextDouble(); //Stops java code and waits for user to input double value for number of acres
     
     System.out.print("Enter the rainfall in inches of the affected area: "); //Asks the user for input on the inches of rainfall
     double inchesOfRainfall = myScanner.nextDouble(); //Stops java code and waits for user to input double value for inches of rainfall
     
     //Calculation Constants
     final double sqFeetPerAcre = 43560; //Enters constant for the number of square feet in an acre
     final double feetPerMile = 5280; //Enters constant for the number of feet in a mile 
     final double inchesPerFoot = 12; //Enters constant for the number of inches in a foot
     final double sqMilesPerAcre = sqFeetPerAcre / Math.pow(feetPerMile, 2); //Calculates constant for the square miles in an acre
     
     //Calculations
     float cubicMilesOfRainfall; //Declares output variable for the cubic miles of rainfall that will be calculated
     
     double feetOfRainfall = inchesOfRainfall / inchesPerFoot; //Calculates the feet of rainfall by dividing the inches of rainfall by the number of inches in a foot
     double milesOfRainfall = feetOfRainfall / feetPerMile; //Calculates the miles of rainfall by dividing the feet of rainfall by the number of feet per mile
       
     double sqMilesAffected = sqMilesPerAcre * acresAffected; //Calculates the square miles affected by multiplying the the square miles per acre times acres affected
     
     cubicMilesOfRainfall = (float) sqMilesAffected * (float) milesOfRainfall; //Calculates cubic miles of rainfall by multiplying square miles affected times the miles of rainfall
     

     //Output
     System.out.println("It rained " + cubicMilesOfRainfall + " cubic miles."); //Output line

   } //end of main method
} //end of class