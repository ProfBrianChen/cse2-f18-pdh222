import java.util.Scanner;

///////////
//// CSE02 HW03 Pyramid.java
/// Peter Hartshorne
/// 9/16/18
/// Obtains information on the dimensions of a square pyramid from the user and calculates its volume.
public class Pyramid {
  // main method required for every Java program
   public static void main(String args[]) {
     //User Inputs
     Scanner myScanner = new Scanner( System.in ); //Declares new scanner
     
     System.out.print("Enter the length of the square side of the pyramid: "); //Asks the user for input on the length of square side of the pyramid
     double length = myScanner.nextDouble(); //Stops java code and waits for user to input double value for length
     
     System.out.print("Enter the height of the pyramid: "); //Asks the user for input on the height of the pyramid
     double height = myScanner.nextDouble(); //Stops java code and waits for user to input double value for height
     
     //Calculations
     double volume; //Declares output variable for the volume of the pyramid that is to be calulcated
     
     double base = Math.pow(length, 2); //The area of the base of the square pyramid is equal to the length squared
     
     volume = (base * height) / 3; //The volume of a square pyramid is equal to the base times the height, divided by 3
       
     //Output
     System.out.println("The volume of the pyramid is " + volume + "."); //Output line

   } //end of main method
} //end of class