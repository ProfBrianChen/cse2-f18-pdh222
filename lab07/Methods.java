import java.util.Scanner;

///////////
//// CSE 02 Lab07 Methods.java
/// Peter Hartshorne
/// 10/25/18
/// Creates 4 different methods to make a sentence
public class Methods {
  // main method required for every Java program
   public static void main(String args[]) {
  String subject = Methods.Subject();
  String sentence = "The " + Methods.Adjective() + " " + subject + " " + Methods.Verb() + " the " + Methods.Adjective() + " " + Methods.Objects() + ".";
   System.out.println(sentence);
   int n = 0;
   n = (int)(Math.random()*4) + 1;
   int i = 0;
   while (i < n){
   System.out.println(Methods.ActionSentence(subject));
   i++;
   }
   System.out.println(Methods.Conclusion(subject));
   }
  
  public static String ActionSentence(String subject){
    int n = 0;
    n = (int)(Math.random()*2);
    String action = "";
     switch ( n ){
       case 0:
        action = "This " + subject + " was " + Methods.Adjective() + " to " + Methods.Adjective() + " " + Methods.Objects() + ".";
        break;
       case 1:
        action = "It " + Methods.Verb() + " the " + Methods.Objects() + " for the " + Methods.Adjective() + " " + Methods.Objects() + ".";
        break;
     }
    return action;
  }
  
  public static String Conclusion(String subject){
    String conclusion = "What a " + Methods.Adjective() + " " + subject + "!";
    return conclusion;
  }
  
  public static String Adjective( ) {
    int n = 0;
    String val = "";
    n = (int)(Math.random()*10);
    switch ( n ){
      case 0:
        val = "green";
        break;
      case 1:
        val = "fearsome";
        break;
      case 2:
         val = "crazy";
         break;
      case 3:
         val = "strong";
         break;
      case 4:
         val = "weak";
         break;
      case 5:
         val =  "old";
         break;
      case 6:
         val = "big";
         break;
      case 7:
         val = "small";
         break;
      case 8:
         val = "wild";
         break;
      case 9:
         val = "smelly";
         break;
    }
    return val;
  }
     
  public static String Subject( ){
    int n = 0;
    String val = "";
    n = (int)(Math.random()*10);
    switch ( n ){
      case 0:
        val = "cat";
        break;
      case 1:
        val = "dog";
        break;
      case 2:
         val = "bear";
         break;
      case 3:
         val = "beast";
         break;
      case 4:
         val = "man";
         break;
      case 5:
         val =  "woman";
         break;
      case 6:
         val = "child";
         break;
      case 7:
         val = "lawyer";
         break;
      case 8:
         val = "teacher";
         break;
      case 9:
         val = "worker";
         break;
  }
    return val;
  } 
  public static String Verb( ){
    int n = 0;
    String val = "";
    n = (int)(Math.random()*10);
    switch ( n ){
      case 0:
        val = "kicked";
        break;
      case 1:
        val = "punched";
        break;
      case 2:
         val = "threw";
         break;
      case 3:
         val = "ate";
         break;
      case 4:
         val = "destroyed";
         break;
      case 5:
         val =  "ripped";
         break;
      case 6:
         val = "felt";
         break;
      case 7:
         val = "smelled";
         break;
      case 8:
         val = "touched";
         break;
      case 9:
         val = "saw";
         break;
  }
    return val;
  }
  public static String Objects( ){
    int n = 0;
    String val = "";
    n = (int)(Math.random()*10);
    switch ( n ){
      case 0:
        val = "sheets";
        break;
      case 1:
        val = "beds";
        break;
      case 2:
         val = "notebooks";
         break;
      case 3:
         val = "computers";
         break;
      case 4:
         val = "lamps";
         break;
      case 5:
         val =  "toilets";
         break;
      case 6:
         val = "shoes";
         break;
      case 7:
         val = "doors";
         break;
      case 8:
         val = "desks";
         break;
      case 9:
         val = "chairs";
         break;
    }
    return val;
      }
        }
          