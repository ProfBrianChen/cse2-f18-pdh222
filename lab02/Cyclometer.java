///////////
/// CSE 02 Lab 02 Cyclometer
/// Peter Hartshorne
/// 9/6/18
/// Records bicycle trip time (s) and # of rotations of the tires and prints trip data based on these values.
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      //Inputs
      int secsTrip1=480;  //time in seconds of trip 1
      int secsTrip2=3220;  //time in seconds of trip 2
      int countsTrip1=1561;  //# of rotations in trip 1
      int countsTrip2=9037; //# of rotations in trip 2

      //Constants
      double wheelDiameter=27.0,  //diameter of the wheel in inches
  	  PI=3.14159, //value for pi
  	  feetPerMile=5280,  //number of feet in a mile
  	  inchesPerFoot=12,   //number of inches per foot
  	  secondsPerMinute=60;  //number of seconds per minute
	    double distanceTrip1, distanceTrip2, totalDistance;  //declaring trip distance variables to be calculated
      
      //Time and # of Rotations Output
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); //trip 1 time (mins) and # of rotations
	    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); //trip 2 time (mins) and # of rotations
        
	    //Calculations
	    //Calculates distance in inches based on # of wheel rotations
	    distanceTrip1=countsTrip1*wheelDiameter*PI;
        //for every rotation of the wheel, the bike travels the circumference of the wheel (pi*d)
     	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
     	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //runs same calculation for trip 2
      totalDistance=distanceTrip1+distanceTrip2; //adds both trip distances to get total distance
      
      //Calculated Outputs for Trip Distance
      System.out.println("Trip 1 was "+distanceTrip1+" miles"); //Prints distance of trip 1 in miles
	    System.out.println("Trip 2 was "+distanceTrip2+" miles"); //Prints distnace of trip 2 in miles
	    System.out.println("The total distance was "+totalDistance+" miles"); //Prints total distance in miles
      
	}  //end of main method   
} //end of class