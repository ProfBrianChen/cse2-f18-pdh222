///////////
//// CSE02 Lab04 CardGenerator
/// Peter Hartshorne
/// 9/20/18
/// Generates a random card from a deck and reports it to the user.
public class CardGenerator {
  // main method required for every Java program
   public static void main(String args[]) {
     
     int card = (int)(Math.random()*51)+1; //Generates a random number between 1 and 52 (inclusive)
     System.out.println(card); //Prints out the number generated in order to check for accuracy in the output
     String suit = ""; //Declares string suit that will be assigned a string by calculations
     String identity = ""; //Declares string identity that will be assigned a string by calculations
     
     //Suit Calculations
     if (1 <= card && card <= 13) { //If the card generated is between 1 and 13 then it is a diamond
       suit = "Diamonds"; //Assigns suit diamonds to "diamonds"
       }
     
     if (14 <= card && card <= 26){ //If the card generated is between 14 and 26 then it is a club
       suit = "Clubs"; //Assigns suit variable to "clubs"
     }
     
     if (27 <= card && card <= 39){ //If the card generated is between 27 and 39 then it is a heart
       suit = "Hearts"; //Assigns suit variable to "hearts"
       }
     
    if (40 <= card && card <= 52){ //If the card generated is between 40 and 52 then it is a spade
      suit = "Spades"; //Assigns suit variable to "spades"
     }
      
     //Identity Calculations
     card = card % 13; //Takes remainder of card / 13 to get the identity of the card
     switch( card ){
       case 11:
         identity = "Jack"; //Assigns identity to "jack"
         System.out.println("You picked the " + identity + " of " + suit); //Output
         break;
       case 12:
         identity = "Queen"; //Assigns identity to "queen"
         System.out.println("You picked the " + identity + " of " + suit); //Output
         break;
       case 0:
         identity = "King"; //Assigns identity to "king"
         System.out.println("You picked the " + identity + " of " + suit); //Output
         break;
       case 1:
         identity = "Ace"; //Assigns identity to "ace"
         System.out.println("You picked the " + identity + " of " + suit); //Output
         break;
           default:
             System.out.println("You picked the " + card + " of " + suit); //Outputs default for non-face cards
     }
    
   } //end of main method
} //end of class