///////////
//// CSE 02 WelcomeClass
///
public class WelcomeClass{
  
   public static void main(String args[]){
     //prints a short autobiography
     System.out.println("-----------");     
     System.out.println("| WELCOME |");
     System.out.println("-----------");
     System.out.println(" ^  ^  ^  ^  ^  ^");
     System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
     System.out.println("<-P--D--H--2--2--2->");
     System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
     System.out.println(" v  v  v  v  v  v");
     System.out.println("Hello. My name is Peter.\nI love working on small, hands-on projects!\nI have recently built a computer, guitar, and wired my own car audio system!");
                       
   }
  
}    