import java.util.Scanner;

///////////
//// CSE 02 Lab03 Check.java
/// Peter Hartshorne
/// 9/13/18
/// Obtains information on restaurant bill from user and calculates the bill based on this information.
public class Check {
  // main method required for every Java program
   public static void main(String args[]) {
     //User Inputs
     Scanner myScanner = new Scanner( System.in ); //Declares new scanner
     System.out.print("Enter the original cost of the check in the form xx.xx: "); //Asks the user for input on cost of check
     double checkCost = myScanner.nextDouble(); //Stops java code and waits for user to input double value for check cost
     
  	 System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //Asks the user for the tip percentage
     double tipPercent = myScanner.nextDouble(); //Stops java code and waits for user to input double value for tip percentage
     tipPercent /= 100; //We want to convert the percentage into a decimal value
       
     System.out.print("Enter the number of people who went out to dinner: "); //Asks the user for input on the number of people paying
     int numPeople = myScanner.nextInt(); ////Stops java code and waits for user to input integer value for the number of people
     
     
     //Calculations
     double totalCost; //Declares output variable for totalCost that will be calculated
     double costPerPerson; //Declared output variable for costPerPerson that will be calculated
        int dollars, dimes, pennies; //Decalres integer variables for the number of dollars, dimes, and pennies that will be used to report total cost
     
     totalCost = checkCost * (1 + tipPercent); //The total cost is equal to the cost of the check times (1 + the tip percent)
     costPerPerson = totalCost / numPeople; //The cost per person is equal to the total cost divided by the number of people paying
     dollars=(int)costPerPerson; //Gets the whole amount, dropping decimal fraction
     dimes=(int)(costPerPerson * 10) % 10; //The % operator finds whole number of dimes by finding the remainder of the (cost per person * 10) / 10
     pennies=(int)(costPerPerson * 100) % 10; ////The % operator finds whole number of pennies by finding the remainder of the (cost per person * 100) / 10
     
     //Output
     System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);




     
   } //end of main method
} //end of class