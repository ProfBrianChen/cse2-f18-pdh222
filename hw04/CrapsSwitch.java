import java.util.Scanner;

///////////
//// CSE 02 HW04 CrapsSwitch.java
/// Peter Hartshorne
/// 9/23/18
/// Gives the slang terminologoy that corresponds to two dice rolls using switch statements
public class CrapsSwitch {
  // main method required for every Java program
   public static void main(String args[]) {
     //User Inputs
     Scanner myScanner = new Scanner( System.in ); //Declares new scanner
     System.out.println("Would you like to generate two random dice or state them yourself? (type either 'generate' or 'state'): ");
     String decision = myScanner.next(); //Creates String variable decision that is given the value of whatever the user types
     
     int roll1 = 0;
     int roll2 = 0;
     switch ( decision ) {
       case "generate":
        roll1 = (int)(Math.random()*6 + 1); //If it is equal, generate two random rolls
          System.out.println("Roll number one is a " + roll1);
        roll2 = (int)(Math.random()*6 + 1);
          System.out.println("Roll number two is a " + roll2);
        break;
       
       case "state": //Checks if the decision is equal to state
       System.out.println("What would you like the first dice roll to be? (state an integer from 1-6): "); //if decision equals state, the user reports two rolls
         roll1 = myScanner.nextInt();
       System.out.println("What would you like the second dice roll to be? (state an integer from 1-6): ");
         roll2 = myScanner.nextInt();
       break;
        default:
       System.out.println("Sorry, that was not an accepted response, please rerun the program to try again."); //Output if the user prints something wrong
     } //end of first switch statement
     
      switch ( roll1 ){ //Goes through all possible rolls using switch statements
        case 1:
          switch ( roll2 ){
            case 1:
                System.out.println("Your roll is a Snake Eyes");
                break;
            case 2:
                System.out.println("Your roll is an Ace Deuce");
                break;
            case 3:
                System.out.println("Your roll is an Easy Four");
                break;
            case 4:
                System.out.println("Your roll is a Fever Five");
                break;
            case 5:
                System.out.println("Your roll is an Easy Six");
                break;
            case 6:
                System.out.println("Your roll is a Seven Out");
                break;
          }
          break;
        case 2:
          switch ( roll2 ){
              case 1:
                System.out.println("Your roll is an Ace Deuce");
                break;
            case 2:
                System.out.println("Your roll is a Hard Four");
                break;
            case 3:
                System.out.println("Your roll is a Fever Five");
                break;
            case 4:
                System.out.println("Your roll is a Easy Six");
                break;
            case 5:
                System.out.println("Your roll is a Seven Out");
                break;
            case 6:
                System.out.println("Your roll is a Easy Eight");
                break;
            }
            break;
              case 3:
          switch ( roll2 ){
              case 1:
                System.out.println("Your roll is an Easy Four");
                break;
            case 2:
                System.out.println("Your roll is a Fever Five");
                break;
            case 3:
                System.out.println("Your roll is a Hard Six");
                break;
            case 4:
                System.out.println("Your roll is a Seven Out");
                break;
            case 5:
                System.out.println("Your roll is an Easy Eight");
                break;
            case 6:
                System.out.println("Your roll is a Nine");
                break;
            }
            break;  
              case 4:
          switch ( roll2 ){
              case 1:
                System.out.println("Your roll is a Fever Five");
                break;
            case 2:
                System.out.println("Your roll is an Easy Six");
                break;
            case 3:
                System.out.println("Your roll is a Seven Out");
                break;
            case 4:
                System.out.println("Your roll is a Hard Eight");
                break;
            case 5:
                System.out.println("Your roll is a Nine");
                break;
            case 6:
                System.out.println("Your roll is a Easy Ten");
                break;
            }
            break;  
              case 5:
          switch ( roll2 ){
              case 1:
                System.out.println("Your roll is an Easy Six");
                break;
            case 2:
                System.out.println("Your roll is a Seven Out");
                break;
            case 3:
                System.out.println("Your roll is an Easy Eight");
                break;
            case 4:
                System.out.println("Your roll is a Nine");
                break;
            case 5:
                System.out.println("Your roll is a Hard Ten");
                break;
            case 6:
                System.out.println("Your roll is a Yo-leven");
                break;
            }
            break;  
              case 6:
          switch ( roll2 ){
              case 1:
                System.out.println("Your roll is a Seven Out");
                break;
            case 2:
                System.out.println("Your roll is an Easy Eight");
                break;
            case 3:
                System.out.println("Your roll is a Nine");
                break;
            case 4:
                System.out.println("Your roll is an Easy Ten");
                break;
            case 5:
                System.out.println("Your roll is a Yo-leven");
                break;
            case 6:
                System.out.println("Your roll is a Boxcars");
                break;
            }
            break;  
      
       } //end of second main switch statement
     
   } //end of main method
  
} //end of class