import java.util.Scanner;

///////////
//// CSE 02 HW04 CrapsIf.java
/// Peter Hartshorne
/// 9/23/18
/// Gives the slang terminologoy that corresponds to two dice rolls using if statements
public class CrapsIf {
  // main method required for every Java program
   public static void main(String args[]) {
     //User Inputs
     Scanner myScanner = new Scanner( System.in ); //Declares new scanner
     System.out.println("Would you like to generate two random dice or state them yourself? (type either 'generate' or 'state'): ");
     String decision = myScanner.next(); //Creates String variable decision that is given the value of whatever the user types
     
     if (decision.equals("generate")){ //Checks if decision is equal to the string "generate"
      int roll1 = (int)(Math.random()*5)+1; //If it is equal, generate two random rolls
      System.out.println("Roll number one is a " + roll1);
      int roll2 = (int)(Math.random()*5)+1;
      System.out.println("Roll number two is a " + roll2);
       
       //Reports Rolls
       if (roll1 + roll2 == 2){
         System.out.println("Your roll is a Snake Eyes");
       }
       
       else if (roll1 + roll2 == 3){
         System.out.println("Your roll is an Ace Deuce");
       }
       
       else if ((roll1 + roll2 == 4) && (roll1 != roll2)){ //If roll 1 + roll 2 is equal to 4, and they aren't equal, it has to be an easy four
         System.out.println("Your roll is an Easy Four");
       }
       
       else if (roll1 + roll2 == 5){
         System.out.println("Your roll is a Fever Five");
       }
       
       else if ((roll1 + roll2 == 6) && (roll1 != roll2)){
         System.out.println("Your roll is an Easy Six");
       }
       
       else if (roll1 + roll2 == 7){
         System.out.println("Your roll is a Seven Out");
       }
       
       else if (roll1 + roll2 == 4 && roll1 == roll2){
         System.out.println("Your roll is a Hard Four");
       }
       
       else if ((roll1 + roll2 == 8) && (roll1 != roll2)){
         System.out.println("Your roll is an Easy Eight");
       }
       
       else if (roll1 + roll2 == 6 && roll1 == roll2){
         System.out.println("Your roll is a Hard Six");
             }
       
       else if (roll1 + roll2 == 9){
         System.out.println("Your roll is a Nine");
             }
       
        else if (roll1 + roll2 == 8 && roll1 == roll2){
         System.out.println("Your roll is a Hard Eight");
             }
       
       else if ((roll1 + roll2 == 10) && (roll1 != roll2)){
         System.out.println("Your roll is an Easy Ten");
             }
      
       else if (roll1 + roll2 == 10 && roll1 == roll2){
         System.out.println("Your roll is a Hard Ten");
             }
       
       else if (roll1 + roll2 == 11){
         System.out.println("Your roll is a Yo-leven");
             }
       
       else if (roll1 + roll2 == 12){
         System.out.println("Your roll is a Boxcars");
             }
     }
     
     else if (decision.equals("state")){ //Checks if the decision is equal to state
       System.out.println("What would you like the first dice roll to be? (state an integer from 1-6): "); //if decision equals state, the user reports two rolls
         int roll1 = myScanner.nextInt();
       System.out.println("What would you like the second dice roll to be? (state an integer from 1-6): ");
         int roll2 = myScanner.nextInt();
       
      if (roll1 >= 1 && roll1 <= 6 && roll2 >= 1 && roll2 <= 6){ //only reports the name of a roll if the user reports the rolls within the correct bounds
       
      if (roll1 + roll2 == 2){
         System.out.println("Your roll is a Snake Eyes");
       }
       
       else if (roll1 + roll2 == 3){
         System.out.println("Your roll is an Ace Deuce");
       }
       
       else if (roll1 + roll2 == 4 && roll1 != roll2){
         System.out.println("Your roll is an Easy Four");
       }
       
       else if (roll1 + roll2 == 5){
         System.out.println("Your roll is a Fever Five");
       }
       
       else if (roll1 + roll2 == 6 && roll1 != roll2){
         System.out.println("Your roll is an Easy Six");
       }
       
       else if (roll1 + roll2 == 7){
         System.out.println("Your roll is a Seven Out");
       }
       
       else if (roll1 + roll2 == 4 && roll1 == roll2){
         System.out.println("Your roll is a Hard Four");
       }
       
       else if (roll1 + roll2 ==8 && roll1 != roll2){
         System.out.println("Your roll is an Easy Eight");
       }
       
       else if (roll1 + roll2 == 6 && roll1 == roll2){
         System.out.println("Your roll is a Hard Six");
             }
       
       else if (roll1 + roll2 == 9){
         System.out.println("Your roll is a Nine");
             }
       
        else if (roll1 + roll2 == 8 && roll1 == roll2){
         System.out.println("Your roll is a Hard Eight");
             }
       
       else if (roll1 + roll2 == 10 && roll1 != roll2){
         System.out.println("Your roll is an Easy Ten");
             }
      
       else if (roll1 + roll2 == 10 && roll1 == roll2){
         System.out.println("Your roll is a Hard Ten");
             }
       
       else if (roll1 + roll2 == 11){
         System.out.println("Your roll is a Yo-leven");
             }
       
       else if (roll1 + roll2 == 12){
         System.out.println("Your roll is a Boxcars");
             }
          }
       else {
         System.out.println("Sorry; one of your rolls was not within the range. Restart the code to try again"); //Output if the reported rolls are not within the bounds
       }
     }
     
     else {
       System.out.println("Sorry, that was not an accepted response, please rerun the program to try again."); //Output if the user prints something other then generate or state
     }


   } //end of main method
} //end of class