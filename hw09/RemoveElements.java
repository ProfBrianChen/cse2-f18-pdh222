///////////
//// CSE02 HW09 CSE2Linear
/// Peter Hartshorne
/// 11/27/18
/// Creates an array and preforms functions on it to modify it.
import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput(num);
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y")); //while answer is y repeat code
  }
 
  public static String listArray(int num[]){ //method for listarray
	String out="{";
	for(int j=0;j<num.length;j++){ //iterates through array
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }

public static int[] randomInput(int[] a){ //method to generate random input for array
  for (int i = 0; i < a.length; i++){ //iterates through the given array
    int temp = (int)(Math.random() * 9); //generates a random number between 0 and 9
    a[i] = temp; //sets random number equal to a[i]
  }
  return a; //returns array a
}
  
public static int[] delete(int[] a, int pos){ //method to delete a position
  int[] temp = new int[a.length-1]; //creates a temporary array of one less length because removing one member of original array
  if (pos < a.length){ //if the pos variable is less than the length of the array the code will run
  for (int i = 0; i < pos; i++){ //iterates through part of the array less than position
    temp[i] = a[i]; //sets temp array equal to original array
  }
  for (int j = pos; j < a.length; j++){ //skips over the part of the array at the pos
   temp[j-1] = a[j]; 
  }
  return temp;
}
else{
  System.out.println("Pos outside bounds, array unchanged");
  return a;
}
} 
public static int[] remove(int[] a, int target){ //int to remove target values from the array
  int counter = 0;
  for (int i = 0; i < a.length; i++){
    if (a[i] == target){ //sets up a counter variable that increases every time the target value is found
      counter++;
    }
  } //this will determine the length of the new array
  int[] temp = new int[a.length]; //creates temp1 array that stores all of the values except the target values
  for (int j = 0; j < a.length; j++){
    if (a[j] == target){}
    else{
      temp[j] = a[j];
    }
  }
  int[] temp2 = new int[a.length-counter]; //creates temp2 array of the correct length and copies the correct values from temp1
  for (int k = 0; k < temp2.length; k++){
      temp2[k] = temp[k];
    }
  return temp2;
  }
  
}

