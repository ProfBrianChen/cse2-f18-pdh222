///////////
//// CSE02 HW09 CSE2Linear
/// Peter Hartshorne
/// 11/27/18
/// Creates an array of grades and uses binary + linear searching to look for a specific grade in the array.
import java.util.Scanner;
//main method
public class CSE2Linear{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); //initializes scanner.
System.out.println("Please enter 15 integers for grades from 0-100 in ascending order");
int[] array = new int[ 15 ]; //declares array of ints
int temp = 0;
int i = 1;
boolean correct = false;
while (correct == false){ //while correct is false continue accepting input to store into array
  if (scan.hasNextInt()){ //if the user enters an integer
    temp = scan.nextInt(); //temp = the integer entered
    if (temp < 0 || temp > 100){ //must be from 0-100
      System.out.println("Variable outside of bounds");
    }
    else{
      //System.out.println("i = " + i);
        if (temp < array[i-1]){ //if the integer entered is not ascending it discards it
        System.out.println("Not ascending");
        }
        else{
          array[i -1] = temp; //otherwise it stores the integer in teh array
          i++;
      if (i == 16){
        correct = true; //stops the code once i has iterated 15 times
      }
        }
    }
  }
  else{
    System.out.println("Not an integer"); //if the user enters an integer it iterates through the input
    scan.next();
  }
  }
  printArray(array); //calls method to print array
  System.out.println("Enter a target to search for using binary searching:");
  int target = scan.nextInt(); //gets input for target
  System.out.println("Target was " + binarySearch(array, target)); //true means that target was found
  System.out.println("Randomly scrambling" + randomScramble(array)); //scrambles array
  printArray(array); //prints scrabled array
  System.out.println("Enter another grade to search for using linear searching:");
  int target2 = scan.nextInt(); //finds input for target
  System.out.println("Target was " + linearSearch(array, target2)); //true means target was found
}

  public static boolean linearSearch(int[] a, int target){ //method for linear search
   int counter = 0; //counter stores number of iterations until either target is found or not found
    for (int i = 0; i < a.length; i++){ //iterates through array
      counter++;
      if (a[i] == target){
        System.out.println("Found after " + counter + " iterations"); //if the target is found, return true and the number of iterations
        return true;
      }
    }
    return false;
  }
  
  public static int[] randomScramble(int a[]){ //method for randomScramble
    for (int i = 0; i < a.length; i++){ //iterates through array
      int random = (int) (Math.random() * a.length); //finds a random index of the array to swap with
      int temp = a[i]; //sets temp variable = to a[i]
      a[i] = a[random]; //sets a[i] equal to random index
      a[random] = temp;  //sets random index = to temp which was set equal to a[i]
    }
    return a; //returns array a
  }
 
 public static boolean binarySearch(int a[], int target){ //method for binarySearch
   int left = 0; //sets left bound to min index (0)
   int right = a.length; //sets right bound to max index
   int counter = 0; //sets counter to 0
   for (int i = 0; i < a.length; i++){ //iterates through array
     int mid = (left + right)/2; //finds midpoint between left and right bounds
     if (mid == target){ 
       counter++;
       System.out.println("Number of iterations: " + counter); //if the midpoint is equal to the target then return true and number of iterations
       return true;
              }
     else if (target < mid){ //if the target is less than the midpoint, set the right bound equal to the midpoint - 1
       right = mid - 1;
       counter++;
     }
     else{ //if the target is greater than the midpoint, set the left bound equal to the midpoint + 1
       left = mid + 1;
       counter++;
     }
   }
   System.out.println("Number of iterations: " + counter);
   return false;
 }
   
 public static int[] printArray(int a[]){ //method to  printArray
   for (int i = 0; i < a.length; i++){ //iterates through array
     System.out.print(a[i] + " "); //prints out every member of a
   }
   System.out.println();
   return a;
 }
    
    
} 